<?php
/**
 * Copyright (C) 2018 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\VuFindHttpPsrCompat;

use Psr\Http\Client\ClientInterface;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Zend\Diactoros\RequestFactory;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\StreamFactory;
use Zend\Diactoros\UploadedFileFactory;
use Zend\Diactoros\UriFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

class Module
{
    public function getConfig(): array
    {
        return [
            'service_manager' => [
                'factories' => $this->getServiceFactories(),
                'aliases'   => $this->getServiceAliases(),
            ]
        ];
    }

    protected function getServiceFactories(): array
    {
        return [
            ClientInterface::class      => Factory::class,
            ResponseFactory::class      => InvokableFactory::class,
            RequestFactory::class       => InvokableFactory::class,
            ServerRequestFactory::class => InvokableFactory::class,
            StreamFactory::class        => InvokableFactory::class,
            UploadedFileFactory::class  => InvokableFactory::class,
            UriFactory::class           => InvokableFactory::class,
        ];
    }

    protected function getServiceAliases(): array
    {
        return [
            ResponseFactoryInterface::class      => ResponseFactory::class,
            RequestFactoryInterface::class       => RequestFactory::class,
            ServerRequestFactoryInterface::class => ServerRequestFactory::class,
            StreamFactoryInterface::class        => StreamFactory::class,
            UploadedFileFactoryInterface::class  => UploadedFileFactory::class,
            UriFactoryInterface::class           => UriFactory::class,
        ];
    }
}